# Running AutoSD on Raspberry pi 4

The Automotive SIG publishes images built via [OSBuild](https://www.osbuild.org/)
based on the OSBuild manifest present in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

Here is a quick guide on how to get you started with them.

We recommend you download the `gadget` image to start with (see our
[Download images](../download_images.md) page for more information about the
different images built).


1. Update eeprom, we have noticed some failures to boot on rpi4's with out of date eeprom. See bottom of page for guide on how to update eeprom.
1. Download your rpi4 image from [https://autosd.sig.centos.org/AutoSD-9/nightly/](https://autosd.sig.centos.org/AutoSD-9/nightly/)
1. Unpack it.

    ```
    unxz auto-osbuild-rpi4-cs9-gadget-regular-aarch64-*.raw.xz
    ```

1. Flash it onto your SD card.

    !!! important

        Change the block device, shown as _`/dev/disk0`_ in the example, according to your system.


         ```
         sudo dd if=auto-osbuild-rpi4-cs9-gadget-regular-aarch64-*.raw of=/dev/disk0 status=progress bs=4M; sync
         ```


1. Insert the SD card into your Raspberry Pi 4.
1. Boot the Raspberry Pi 4.
1. Connect the Raspberry Pi 4.

   !!! important serial and wifi is not yet functional.

        Connect to the Raspberry Pi 4 via USB keyboard or via ssh with an ethernet cable

1. Login as `root` or `guest` using the password: `password`.
1. Connect the Raspberry Pi 4 to the internet.

     Option 1)  
     If you have an ethernet cable, it will be the easiest solution.

     Option 2)  
     To enable the WiFi adapter on the Raspberry Pi 4 first we need to install and enable the NetworkManager-wifi package, then restart NetworkManager:
     
        # dnf install -y NetworkManager-wifi
        # systemctl restart NetworkManager

     After that you should be able to list surrounding WiFi SSIDs and connect to them:  

        # nmcli dev wifi list
        # nmcli --ask dev wifi connect <some-wifi-ssid>

     Option 3)  
     If you are not near to a router or cannot connect the pi to an ethernet cable, this is where
     the `gadget` image is interesting, see our page on [USB gadget](gadget.md) for more information
     on how to get it working.

1. Profit

## Update eeprom

Raspberry Pi OS comes with a utility to update eeprom. Once Raspberry Pi OS is installed, the utility can be installed with the following commands:

    sudo apt update
    sudo apt full-upgrade
    sudo apt install rpi-eeprom
    sudo reboot

Checking for Updates Manually:

    sudo rpi-eeprom-update

This will produce output like this if there is an update available, CURRENT and LATEST won't match:

    BOOTLOADER: update available
    CURRENT: Thu 3 Sep 12:11:43 UTC 2020 (1599135103)
    LATEST: Tue 24 Nov 15:08:04 UTC 2020 (1606230484)
    FW DIR: /lib/firmware/raspberrypi/bootloader/beta
    VL805: up-to-date
    CURRENT: 000138a1
    LATEST: 000138a1

To install this update use the -a switch as well as the -d switch (which means to check the bootloader):

    sudo rpi-eeprom-update -d -a
    sudo reboot

## Enable bluetooth

To enable bluetooth functionality, first we'll need to install some required packages:

        # dnf install NetworkManager-bluetooth
        # systemctl restart NetworkManager

Next, enable and start the service:

        # systemctl enable --now bluetooth

Now, if we check 'bluetoothctl list', you'll notice something's wrong here:

        # bluetoothctl list
        Controller AA:AA:AA:AA:AA:AA BlueZ 5.56 [default]

Our device MAC address should not show up as 'AA:AA:AA:AA:AA:AA'

That's because we're missing firmware that linux-firmware does not provide, even though dracut checks for it:

        # cd lib
        # grep -rni . -e firmware/ | grep brcm | grep dracut
        ./dracut/modules.d/62bluetooth/module-setup.sh:37:        /lib/firmware/brcm/*.hcd* \

        # ls /lib/firmware/brcm/
        'brcmfmac43430a0-sdio.ONDA-V80 PLUS.txt.xz'  'brcmfmac43455-sdio.MINIX-NEO Z83-4.txt.xz'            'brcmfmac43455-sdio.Raspberry Pi Foundation-Raspberry Pi 4 Model B.txt.xz'
        brcmfmac43455-sdio.acepc-t8.txt.xz           brcmfmac43455-sdio.raspberrypi,3-model-a-plus.txt.xz  'brcmfmac43455-sdio.Raspberry Pi Foundation-Raspberry Pi Compute Module 4.txt.xz'
        brcmfmac43455-sdio.bin.xz                    brcmfmac43455-sdio.raspberrypi,3-model-b-plus.txt.xz
        brcmfmac43455-sdio.clm_blob.xz               brcmfmac43455-sdio.raspberrypi,4-model-b.txt.xz

Notice there are no *.hcd files for any bluetooth firmware. Luckily we can just download the required firmware from github:

        # cd ~
        # git clone https://github.com/RPi-Distro/bluez-firmware/tree/master/broadcom
        # cd bluez-firmware/broadcom
        # cp *.hcd /lib/firmware/brcm/

Then reboot:

        # reboot

And now we should get a proper mac address for our device:

        # bluetoothctl list
        Controller DC:A6:32:B9:6A:DC BlueZ 5.56 [default]

Go ahead and turn the device on:

        # bluetoothctl
        > power on
        Changing power on succeeded.

Next, put the device you want to pair with the rpi4 into pairing mode -- such as controller or portable speaker, then turn scan on and wait for your device to pop up:

        > scan on
        ...
        [NEW] Device F4:93:9F:63:7F:6C Wireless Controller
        ...

Connect to the device:

        > connect F4:93:9F:63:7F:6C

        [bluetooth]# connect F4:93:9F:63:7F:6C
        Attempting to connect to F4:93:9F:63:7F:6C
        [CHG] Device F4:93:9F:63:7F:6C Connected: yes
        [CHG] Device F4:93:9F:63:7F:6C UUIDs: 00001124-0000-1000-8000-00805f9b34fb
        [CHG] Device F4:93:9F:63:7F:6C UUIDs: 00001200-0000-1000-8000-00805f9b34fb
        [CHG] Device F4:93:9F:63:7F:6C ServicesResolved: yes
        [CHG] Device F4:93:9F:63:7F:6C Paired: yes
        Connection successful

Now your device should be connected and you can exit bluetoothctl

        > exit

